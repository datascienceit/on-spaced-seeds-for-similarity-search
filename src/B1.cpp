#include "../include/B1.h"

B1::B1(string seed)
{
	this->seed = seed;
}

// Recursively calculate all the b suffixes for a given seed
void B1::recSeed(string seed,string suffix,vector<b>* bVec)
{
	string suffix1 = suffix;
	string suffix0 = suffix;

	if(suffix.length() == seed.length()-1)
	{
		if(seed.at(0) == '0')
		{
			suffix1.insert(0,"1");
			suffix0.insert(0,"0");
			bVec->push_back(b(suffix1));
			bVec->push_back(b(suffix0));
		}
		else
		{
			suffix1.insert(0,"1");
			bVec->push_back(b(suffix1));
		}
	}

	else if(seed.at(seed.length()-suffix.length()-1) == '0')
	{
		suffix1.insert(0,"1");
		suffix0.insert(0,"0");
		recSeed(seed,suffix1,bVec);
		recSeed(seed,suffix0,bVec);
		bVec->push_back(b(suffix1));
		bVec->push_back(b(suffix0));
	}
	else
	{
		suffix1.insert(0,"1");
		recSeed(seed,suffix1,bVec);
		bVec->push_back(b(suffix1));
	}
}


// calculate min j for each b such that 0b<<j is in B1
void B1::jMin(int seedLength, vector<b>* bVec)
{
	vector<b> ::iterator veciter;
	for(veciter = bVec->begin(); veciter != bVec->end();++veciter)
	{
		int len = veciter->str.length();
		if(len == seedLength)
			continue;
		bool found = false;
		for(int j=0;j<=len && !found;j++)
		{
			string prefix = "0";
			prefix.append(veciter->str.substr(0,len-j));

			vector<b> ::iterator veciter2;
			for(veciter2 = bVec->begin(); veciter2 != bVec->end();++veciter2)
			{
				if (!prefix.compare(veciter2->str))
				{
					veciter->j = j;
					veciter->bj = prefix;
					found = true;
					break;
				}
			}
		}
	}
}
