//============================================================================
// Name        : bio_mini_project.cpp
// Author      : Yonatan Alexander & Alon Alexander Fixler
// Version     :
// Copyright   : Your copyright notice
// Description :
//============================================================================
#include <iostream>
#include <stdlib.h>
#include <string>
#include "../include/F.h"
using namespace std;

void printUsage()
{
	cout<<"You've inserted a wrong input!"<<endl;
	cout<<"Usage: SpacedSeeds <L> <M_max> <W> <p>"<<endl;
}


// Create a vector of indices to be used later on when creating a seed's permutation
void permIndcies(int offset, int k, vector<int>* seedIndices, vector<vector<int> >* indicesPerm,vector<int>* combination)
{
  if (k == 0)
  {
	indicesPerm->push_back(*combination);
    return;
  }

  for (int i = offset; i <= (signed int)seedIndices->size() - k; i++) {
    combination->push_back(seedIndices->at(i));
    permIndcies(i+1, k-1,seedIndices,indicesPerm,combination);
    combination->pop_back();
  }
}

// Create a permutation for a given seed length and weight
string permutation(vector<int>* combination,int length)
{
	string seed = "";
	vector<int>::iterator iter = combination->begin();
	for(int i=0;i<length;i++)
	{
		if(iter!=combination->end())
		{
			if(*iter == i)
			{
				seed.append("1");
				iter++;
			}
			else
				seed.append("0");
		}
		else
			seed.append("0");
	}

	return seed;
}


int main(int argc,char **argv)
{
	/*
	input: 
           Length L- String length
           m_max - the maximum length of seed
           w (whight) - the number of matches (1) in the seed.
           p - probability for each bit to be 1 (match).

    output: 
           Hit probability of Q
	*/


	if(argc!=5)
	{
		printUsage();
		exit(0);
	}

	cout<<endl;
	cout<< "Start calculating:"<<endl;
    int L = atoi(argv[1]);
	int m_max = atoi(argv[2]);
	int W = atoi(argv[3]);
	double p = atof(argv[4]);
    double ans_prob = 0;
    double temp_prob = 0;
    string ans_seed = "";
    string temp_seed ="";
    string b_0bj = "";
    int seed_size = 0;//the current seed size


    F f = F(L);//our f[i,b] table
    
    cout<< "Loop over all seeds"<<endl;
    cout<< "It may take a few minutes..."<<endl;
    //iterate every possible seed

    vector<int> seedIndices;
	for(int i=0;i<W;i++)
		seedIndices.push_back(i);

    for(int k=W;k<=m_max;k++)		// seed's length
    {
    	vector<vector<int> > indicesPerm;
		vector<int> combination;
		permIndcies(0, W, &seedIndices, &indicesPerm,&combination);

		for(int t=0;t<(signed int)indicesPerm.size();t++)		// seed's permutation
		{
			string seed = permutation(&indicesPerm.at(t),k);
			B1 b1 = B1(seed);
			vector<b> bVec;
			b1.recSeed(seed,"",&bVec);									// go over all possible suffix of the seed
			bVec.push_back(b(""));									// insert an empty suffix
			std::sort(bVec.begin(), bVec.end(), greater<b>());		// sort seed's b vector
			b1.jMin(seed.length(),&bVec);


			//initialize
			vector<b> :: iterator b_iter;
			seed_size = seed.length();
			// initialize F - Let f[i, b] = 0 for all 0 <= i < M and b in B1 for this seed
			f.init(&bVec,m_max,L, p, seed);

			//for i=M to L : M=Q.length L = S.length (input) : M is the seed length, L is the string S length
			 for(int i = seed_size ; i <= L ; i++ )
			 {
			  //for each b in B1 from the longest to the shortest
			   for ( b_iter=bVec.begin();b_iter != bVec.end(); ++b_iter)
			   {
				 if ((signed int)b_iter->str.length()== seed_size){// if |b|=M
				 //f[i,b]=1
				  f.insert(i,b_iter->str,1);

				 }
				 else{
					 //let j>=0 be the least number such that 0b>> j in B1 while 0b>>j is b_0 b_1...b_(l-1-j)
					 //thats mean we need the shortest string bj which in B1
					 int j = b_iter->j;
					 b_0bj= b_iter->bj;

					 //let f[i,0]= ((1-p) x f[i-j,0b>>j]) + (p x f[i,1b])
					 temp_prob = ((1-p)*f.get_value(i-j,b_0bj))+(p*f.get_value(i,'1'+b_iter->str));
					 f.insert(i,b_iter->str,temp_prob);
				 }
			   }//end of b's
			  }//end of i's

			  temp_prob = f.get_value(L,"");
			  if(temp_prob >= ans_prob)
			  {
				  if(temp_prob == ans_prob)
				  {
					  if(seed.compare(ans_seed) < 0)
					  {
						  ans_prob = temp_prob;//updating the max
						  ans_seed = seed;
					  }
				  }
				  else
				  {
					ans_prob = temp_prob;//updating the max
					ans_seed = seed;
				  }
			  }
		}
		seedIndices.push_back(k);
    }

    cout<<endl;
    cout<< "Results: "<<endl;
	cout<< "Seed: "+ans_seed <<endl;
	cout.precision(7);
    cout<< "Probability: " << ans_prob<< endl;
	return 0;
}
