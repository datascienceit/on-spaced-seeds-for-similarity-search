#include <iostream>
#include <vector>
#include "../include/F.h"




F::F(int L) {
	for(int i=0;i<=L;i++){
	  f_table.push_back(map<string,double>());
	}

	this->seed ="";
}

void F::init(vector<b>* b_vector, int M, int L, double p, string define_seed){
    this->seed = define_seed;
    int i=0;
	this->f_table.clear();
    //initialize the map->f[i,b]
    for (i=0; i<M; i++){ // Let f[i, b] = 0 for all 0 <= i < M and b in B1
    	map<string, double> mp = map<string,double>();
		vector<b> ::iterator b_iter;//create iterator

		for (b_iter = b_vector->begin();b_iter != b_vector->end();++b_iter ){
			//f[i][b] = 0;
			mp[b_iter->str] = 0;
		}
		this->f_table[i] = mp;
    }
}

int F::insert(int i, string b, double val){
	(this->f_table[i])[b] = val;
    return 0;
}

double F::get_value(int i, string b){
    return (this->f_table[i])[b];
}


