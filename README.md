#
In this mini project we implemented in C++ the algorithm of Uri Keich, Ming Li, Bin Ma, John Tromp for finding optimal seed from the article �On Spaced Seeds for Similarity Search�

Our implementation was revolving the pseudo code in section 4.1, which we implemented for each seed.
We get as input the following:  
Let S be a random 0-1 string of length, and Q be a seed.
1. L: S length.
2. M_max: the maximum size of Q.
3. W: the weight of Q, mean the numbers of �1� in Q.
4. P: the probability for each bit to be �1�.
The output is:
The seed with weight W and length M, where M <= M_max, that have the highest probability to hit a random string of length L in which each bit is 1 with probability p. 
If there are more than one optimal seed, our output is the lexicographically first optimal seed.

Implementation main ideas:
In our implementation we have two loops, the outer loop running all possible seed�s lengths, and the inner one goes over all possible seeds of that length by recursively computing all permutations of the seed according to its weight and its length.
For each seed our calculation is built on two parts as follow: 
A. Compute B1 for the specific seed:
Recursively calculate all the b-suffixes of each seed while keeping the b-suffixes in a vector data structure.
B. Compute  j=0 such it is the least numbers such that 0b�j?B_1;
* 0b�j Define to be the binary string b_0 b_1�b_(l-1-j) whilel=|b|.

Next, we compute as the algorithm suggests, a matrix F of size 
(L-|Q|+1�number of b?B_1) while keeping the data in a Vector of maps. 
Each map holds the probability as suggested. 

After calculating the seed probability we compare it to the maximum probability found so far, saving the highest probability, if two are equal we save the lexicographically first one.
Finally we return our results. 

Optimization:
Memory optimization: instead of calculating all possible seeds and keeping them in memory while calculating the highest probability, we calculate for each seed its B1 suffix vector, calculate its probability and then discard the seeds memory, and keep only the seed and its probability if it is the highest one.

Runtime optimization: instead of calculating 0b�j?B_1 during the main loop, we calculated it for each b suffix in a pre-processing stage while calculating B1.
