#include <vector>
#include <string>
#include <map>
#include "B1.h"
using namespace std;

class F {
public:
	vector< map<string,double> > f_table;//this is the f[i,b]
	string seed;
	F(int L);
	void init(vector<b>* b_vector, int M, int L, double p, string define_seed);
	int insert(int i, string b, double val);
	double get_value(int i, string b);
};


