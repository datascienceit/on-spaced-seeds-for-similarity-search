#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include "b.h"

using namespace std;




class B1{
public:
	string seed;

	B1(string seed);
	void recSeed(string seed,string suffix,vector<b>* bVec);
	bool compareByLength(const b &p, const b &q);
	void jMin(int seedLength,vector<b>* bVec);
};
